package com.app.vimas.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.vimas.R;

import java.util.ArrayList;

public class MonthEventAdapter extends BaseAdapter {

    ArrayList<String> event_data;
    LayoutInflater mInflater;

    public MonthEventAdapter(Context context, ArrayList<String> event_data) {
        this.event_data = event_data;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return event_data.size();
    }

    @Override
    public Object getItem(int pos) {
        return pos;
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_event_data, null);
            holder.item_event_date = convertView.findViewById(R.id.item_event_date);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.item_event_date.setText(event_data.get(position));
        return convertView;
    }

    private class ViewHolder {
        private TextView item_event_date;
    }
}
