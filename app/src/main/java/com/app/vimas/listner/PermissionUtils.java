package com.app.vimas.listner;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.List;

public class PermissionUtils {

    // add compile 'gun0912.ted:tedpermission:1.0.0' in your gradle file
    //, final PermissionCallback permissionCallback

    private final static String PERMISSION_GUIDE = "If you reject permission, you can not use this service. Please turn on permissions at [Setting] > [Permission]";

    public static void checkPermission(final Activity activity, List<String> listPermission, final PermissionCallback permissionCallback) {

        List<String> listPermissionToAsk = new ArrayList<>();

        for (String permission : listPermission) {
            if (ActivityCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED)
                listPermissionToAsk.add(permission);
        }

        if (listPermissionToAsk.size() > 0) {

            TedPermission.with(activity).setDeniedMessage(PERMISSION_GUIDE)
                    .setPermissions(listPermissionToAsk.toArray(new String[listPermissionToAsk.size()]))
                    .setPermissionListener(new PermissionListener() {

                        @Override
                        public void onPermissionGranted() {
                            permissionCallback.onGrantedAll();
                        }

                        @Override
                        public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                            permissionCallback.onDeny(deniedPermissions);
                        }
                    }).check();
        } else {
            permissionCallback.onGrantedAll();
        }
    }

    public interface PermissionCallback {
        void onGrantedAll();

        void onDeny(ArrayList<String> listDeniedPermission);
    }
}