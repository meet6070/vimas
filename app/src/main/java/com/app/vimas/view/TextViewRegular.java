package com.app.vimas.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.app.vimas.R;


public class TextViewRegular extends android.support.v7.widget.AppCompatTextView {

    public TextViewRegular(Context context) {
        super(context);
        init(context, null);
    }

    public TextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TextViewRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
            String fontName = a.getString(R.styleable.CustomTextView_fontName);

            Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "SanFranciscoDisplay-Regular_0.ttf");
            setTypeface(myTypeface);

            a.recycle();
        }
    }
}
