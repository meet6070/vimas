package com.app.vimas;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.vimas.fragment.CalenderFragment;
import com.app.vimas.fragment.ScanQRFragment;
import com.app.vimas.fragment.VisitorView;
import com.app.vimas.view.CustomTypefaceSpan;

public class MainActivity extends AppCompatActivity {

    //@BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    //@BindView(R.id.toolbar)
    Toolbar toolbar;
    //@BindView(R.id.nav_view)
    NavigationView nav_view;
    //@BindView(R.id.search_bar)
    CardView search_bar;
    //@BindView(R.id.main_view)
    RelativeLayout main_view;
    FrameLayout layout_item;
    private Context context;
    private ActionBar actionBar;
    private ActionBarDrawerToggle toggle;
    private RelativeLayout layout_manu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ButterKnife.bind(this);
        context = this;

        layout_item = findViewById(R.id.layout_item);
        toolbar = findViewById(R.id.toolbar);
        nav_view = findViewById(R.id.nav_view);
        search_bar = findViewById(R.id.search_bar);
        main_view = findViewById(R.id.main_view);
        drawer_layout = findViewById(R.id.drawer_layout);

        initToolbar();
        initDrawerMenu();
    }

    private void initToolbar() {

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        // actionBar.setHomeButtonEnabled(true);
        //actionBar.setTitle(R.string.app_name);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                tv.setGravity(View.TEXT_ALIGNMENT_TEXT_START);
                tv.setTextColor(Color.BLACK);
                Typeface titleFont = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/" + "SanFranciscoDisplay-Regular_0.ttf");
                if (tv.getText().equals(toolbar.getTitle())) {
                    tv.setTypeface(titleFont);
                    tv.setTextSize(22);
                    break;
                }
            }
        }
    }

    private void initDrawerMenu() {

        toggle = new ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        toggle.setDrawerIndicatorEnabled(false);
        drawer_layout.setDrawerListener(toggle);
        toggle.syncState();
        toolbar.setNavigationIcon(R.drawable.menu_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.openDrawer(Gravity.LEFT);
            }
        });

        //After instantiating your ActionBarDrawerToggle
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem item) {
                onItemSelected(item.getItemId());
                return true;
            }
        });

        nav_view.setItemIconTintList(getResources().getColorStateList(R.color.nav_state_list));
        View header = nav_view.getHeaderView(0);


        /*//Close window
        ImageView iv_close = (ImageView) header.findViewById(R.id.iv_close);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.closeDrawers();
            }
        });*/


        layout_manu = (RelativeLayout) header.findViewById(R.id.layout_manu);
        layout_manu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //iv_user_photo.performClick();
            }
        });

        Menu m = nav_view.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/" + "SanFranciscoDisplay-Regular_0.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font, 16), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    public boolean onItemSelected(int id) {
        Intent i;
        switch (id) {

            case R.id.nav_visitor:
                layout_item.removeAllViews();
                layout_item.setVisibility(View.VISIBLE);
                actionBar.setTitle(R.string.visitor);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        VisitorView visitorView = new VisitorView();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.layout_item, visitorView)
                                .addToBackStack(null)
                                .commit();
                        drawer_layout.closeDrawers();

                    }
                }, 500);


                break;

            case R.id.nav_calender:
                layout_item.removeAllViews();
                layout_item.setVisibility(View.VISIBLE);
                actionBar.setTitle(R.string.calender);

                CalenderFragment calenderFragment = new CalenderFragment();
                FragmentManager calenderFragManager = getSupportFragmentManager();
                calenderFragManager.beginTransaction()
                        .replace(R.id.layout_item, calenderFragment)
                        .commit();

                break;

            case R.id.nav_scan:
                layout_item.removeAllViews();
                layout_item.setVisibility(View.VISIBLE);
                actionBar.setTitle(R.string.scanCode);

                ScanQRFragment scanQRFragment = new ScanQRFragment();
                FragmentManager findjobfragmentManager = getSupportFragmentManager();
                findjobfragmentManager.beginTransaction()
                        .replace(R.id.layout_item, scanQRFragment)
                        .commit();
                break;


            default:
                break;
        }
        drawer_layout.closeDrawers();
        return true;
    }
}
