package com.app.vimas;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.vimas.Connection.CheckCompanyCode;
import com.app.vimas.Connection.IConstants;
import com.app.vimas.listner.PermissionUtils;
import com.app.vimas.view.EditTextViewRegular;
import com.app.vimas.view.TextViewRegular;

import java.util.ArrayList;

public class CompanyLogin extends AppCompatActivity implements View.OnClickListener {

    //@BindView(R.id.edt_company_code)
    EditTextViewRegular edt_company_code;

    //@BindView(R.id.txt_login)
    Button txt_login;

    Activity activity;
    ArrayList<String> permissionlist;
    private ProgressDialog progressDialog;
    final Handler uiHandlerLogin = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            switch (msg.what) {
                case 0:
                    permissionlist = new ArrayList<>();
                    permissionlist.add(Manifest.permission.CAMERA);

                    PermissionUtils.checkPermission(activity, permissionlist, new PermissionUtils.PermissionCallback() {
                        @Override
                        public void onGrantedAll() {
                            startActivity(new Intent(activity, MainActivity.class));
                            finish();
                        }

                        @Override
                        public void onDeny(ArrayList<String> listDeniedPermission) {
                            finish();
                        }
                    });

                    break;
                default:
                    break;
            }
        }
    };
    private String response = "";
    final Handler errorHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();

            switch (msg.what) {
                case 0:
                    response = "Image not Uploaded";
                    Toast.makeText(activity, "Image not Uploaded", Toast.LENGTH_SHORT).show();
                    break;
                case IConstants.ERR_INTERAL:
                    response = "Internal Server Error";
                    Toast.makeText(activity, "Internal Server Error", Toast.LENGTH_SHORT).show();
                    break;
                case IConstants.ERR_SERVER_NOT_RESPONSE:
                    response = "Select Image First";
                    Toast.makeText(activity, "Select Image First", Toast.LENGTH_SHORT).show();
                    break;
                case IConstants.ERR_NETWORK_NOT_AVAILABLE:
                    response = "Network Connection Not available";
                    Toast.makeText(activity, "Network Connection Not available", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_login);

        activity = this;
        edt_company_code = findViewById(R.id.edt_company_code);
        txt_login = findViewById(R.id.txt_login);
        txt_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.txt_login:
                showProgressBar();
                login(edt_company_code.getText().toString());
                break;
        }
    }

    private void login(final String code) {
        if (Utils.isNetworkAvailable(CompanyLogin.this)) {
            Thread loadUploadServiceThread = new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        String res = new CheckCompanyCode().checkCompanyCode(activity, code);

                        uiHandlerLogin.sendEmptyMessage(0);
                    } catch (IllegalStateException e) {
                        Log.e(IConstants.DEBUG, Utils.getStackTrace(e));
                        errorHandler.sendEmptyMessage(IConstants.ERR_INTERAL);
                    } catch (Exception e) {
                                /*Log.e(IConstants.DEBUG, Utils.getStackTrace(e));
                                errorHandler.sendEmptyMessage(IConstants.ERR_SERVER_NOT_RESPONSE);*/
                    }


                }
            };
            loadUploadServiceThread.start();
        } else {
            errorHandler.sendEmptyMessage(IConstants.ERR_NETWORK_NOT_AVAILABLE);
        }
    }

    private void showProgressBar() {
        progressDialog = ProgressDialog.show(CompanyLogin.this, IConstants.BLANK_STRING, "Please Wait");

    }
}
