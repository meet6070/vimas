package com.app.vimas.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.vimas.R;
import com.app.vimas.Utils;
import com.app.vimas.model.Vimas;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class VisitorViewFragment extends Fragment {
    RelativeLayout layout_info_panel;
    TextView subtitle;
    Activity activity;

    public static VisitorViewFragment newInstance(RelativeLayout layout_info_panel, TextView subtitle) {
        VisitorViewFragment fragment = new VisitorViewFragment();
        fragment.layout_info_panel = layout_info_panel;
        fragment.subtitle = subtitle;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        activity = getActivity();
        View view = inflater.inflate(R.layout.fragment_visitorview2, container, false);
        parseJson();
        return view;
    }

    private void parseJson() {
        try {
            JSONObject obj = new JSONObject(Utils.loadJSONFromAsset(activity));
            JSONObject m_jobj = obj.getJSONObject("Main");
            JSONArray m_jArry = m_jobj.getJSONArray("fields");
            ArrayList<Vimas> formList = new ArrayList<>();
            HashMap<String, String> m_li;

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONArray innerJsonArray = m_jArry.getJSONObject(i).getJSONArray("field");
                JSONObject jsonObject = innerJsonArray.getJSONObject(i);
                Vimas vimas = new Vimas();


                vimas.setTitle(jsonObject.optString("title"));
                vimas.setId(jsonObject.optString("id"));
                vimas.setRequired(jsonObject.optString("required"));
                vimas.setMaxlength(jsonObject.optString("maxlength"));
                vimas.set_Type(jsonObject.optString("_Type"));
                vimas.set_Name(jsonObject.optString("_Name"));
                vimas.setItems(jsonObject.optString("Items"));

                /*JSONObject jobjitems = jsonObject.optJSONObject("Items");

                if (jobjitems != null) {
                    JSONArray jarItems = jobjitems.optJSONArray("col");
                    for (int j = i; j < jarItems.length(); i++) {
                        vimas.set_Name(jarItems.getJSONObject(j).optString("_Name"));
                        vimas.set__text(jarItems.getJSONObject(j).optString("__text"));
                    }
                }*/
                formList.add(vimas);
            }
            Log.d("Size", "" + formList.size());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
