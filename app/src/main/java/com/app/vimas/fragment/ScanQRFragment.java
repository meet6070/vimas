package com.app.vimas.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.vimas.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.app.Activity.RESULT_OK;

public class ScanQRFragment extends Fragment implements ZXingScannerView.ResultHandler{

    private ZXingScannerView mScannerView;
    private ImageView myImageView;
    private Activity activity;

    public ScanQRFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_scan_qr, container, false);
        activity = getActivity();
        mScannerView = view.findViewById(R.id.surface_view);
        myImageView = view.findViewById(R.id.myImageView);
        myImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScannerView.stopCamera();
                activity.finish();

            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
        List<BarcodeFormat> formats = new ArrayList<>();
        formats.add(BarcodeFormat.QR_CODE);
        mScannerView.setFormats(formats);
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        Log.v("Prints scan results", rawResult.getText());
        Log.v("the scan format", rawResult.getBarcodeFormat().toString());
        Toast.makeText(activity,"Your Scanned CODE is "+rawResult.getText(),Toast.LENGTH_LONG).show();
        mScannerView.stopCamera();
        /*Intent intent = new Intent();
        intent.putExtra("QRCODE", rawResult.getText());
        activity.setResult(RESULT_OK, intent);*/
        //activity.finish();
    }
}
