package com.app.vimas.fragment;

import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.vimas.R;
import com.app.vimas.Utils;
import com.app.vimas.adapter.MonthEventAdapter;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import jp.co.recruit_mp.android.lightcalendarview.LightCalendarView;
import jp.co.recruit_mp.android.lightcalendarview.MonthView;

public class CalenderFragment extends Fragment {

    List<String> mListAppointment;
    boolean isClickEvent;
    boolean isSelected = true;
    SimpleDateFormat fmtDate = new SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.US);
    RelativeLayout layout_info_panel;
    TextView subtitle;
    private TextView someTextView;
    private LightCalendarView calendarView;
    private ListView mLvMonthEvent;
    private Dialog dialogEvent;
    private ArrayList<String> mListDateDetail;
    private SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
    private SimpleDateFormat formatterMonth = new SimpleDateFormat("MMM yyyy", Locale.US);

    public static CalenderFragment newInstance(RelativeLayout layout_info_panel, TextView subtitle) {
        CalenderFragment fragment = new CalenderFragment();
        fragment.layout_info_panel = layout_info_panel;
        fragment.subtitle = subtitle;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_calender, container, false);
        mLvMonthEvent = rootView.findViewById(R.id.lvMonthEvent);

        someTextView = rootView.findViewById(R.id.someTextView);

        calendarView = rootView.findViewById(R.id.calendarView);
        mListDateDetail = new ArrayList<>();
        calendarView.setLayoutParams(new LinearLayout.LayoutParams(
                (int) (Utils.getDeviceWidth(getActivity()) / 1),
                (int) (Utils.getDeviceWidth(getActivity()) / 1.5)));

        mLvMonthEvent.setVisibility(View.VISIBLE);

        //mListAppointment = RealmController.getInstance().getAllAppointments();
        loadView();

        return rootView;
    }

    public void loadView() {

        Calendar calFrom = Calendar.getInstance();
        Calendar calTo = Calendar.getInstance();
        Calendar calNow = Calendar.getInstance();
        calFrom.set(Calendar.MONTH, -100);
        calTo.set(Calendar.MONTH, 200);

        calendarView.setMonthFrom(calFrom.getTime());
        calendarView.setMonthTo(calTo.getTime());
        calendarView.setMonthCurrent(calNow.getTime());

        calendarView.setOnStateUpdatedListener(new LightCalendarView.OnStateUpdatedListener() {

            @Override
            public void onMonthSelected(@NotNull Date date, @NotNull final MonthView monthView) {

                someTextView.setText(formatterMonth.format(date));

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                    }
                }, 1000);
            }

            @Override
            public void onDateSelected(Date date) {
                //mListDateDetail = RealmController.getInstance().queryedWorkOrderDate(formatter.format(date));

                if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    Toast.makeText(getActivity(), fmtDate.format(date), Toast.LENGTH_SHORT).show();
                    //mListDateDetail.clear();
                    mListDateDetail.add(fmtDate.format(date));
                    mLvMonthEvent.setAdapter(new MonthEventAdapter(getActivity(), mListDateDetail));
                }
            }
        });


        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            Calendar cal = Calendar.getInstance();
            cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));

            mListDateDetail.clear();
            //mListDateDetail = RealmController.getInstance().queryedWorkOrderDate(formatter.format(cal.getTime()));

            //mLvMonthEvent.setAdapter(new MonthEventAdapter(getActivity(), mListDateDetail));
        }

        mLvMonthEvent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                isClickEvent = true;
            }
        });


    }
}
