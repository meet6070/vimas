package com.app.vimas.fragment;

import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.vimas.R;
import com.app.vimas.utils.Common;


public class VisitorView extends Fragment implements TabLayout.OnTabSelectedListener {

    //@BindView(R.id.viewPager)
    ViewPager viewPager;
    //@BindView(R.id.tabLayout)
    TabLayout tabLayout;
    /*info pennel*/
    //@BindView(R.id.layout_info_panel)
    RelativeLayout layout_info_panel;
    //@BindView(R.id.subtitle)
    TextView subtitle;
    /*alert pennel*/
    //@BindView(R.id.rlMainView)
    RelativeLayout rlMainView;
    //@BindView(R.id.tvTitle)
    TextView tvTitle;
    Pager adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_visitorview, container, false);

        viewPager = view.findViewById(R.id.viewPager);
        tabLayout = view.findViewById(R.id.tabLayout);
        layout_info_panel = view.findViewById(R.id.layout_info_panel);
        subtitle = view.findViewById(R.id.subtitle);
        rlMainView = view.findViewById(R.id.rlMainView);
        tvTitle = view.findViewById(R.id.tvTitle);

        initData();

        return view;
    }

    private void initData() {

        Common.mainActivity = getActivity();
        Common.rlEditMainView = rlMainView;
        Common.tvEditTitle = tvTitle;
        Common.editviewPager = viewPager;


        //Tab-Layout
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.visitor)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.host)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.declare)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //coding for add divider in tabLayout

        LinearLayout linearLayout = (LinearLayout) tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(getResources().getColor(R.color.custom_gray));
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(20);
        linearLayout.setDividerDrawable(drawable);

        changeTabsFont();

        adapter = new Pager(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(this);
        setupTabIcons();
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabOne.setText(getString(R.string.visitor));
        // tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.driveractive, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabTwo.setText(getString(R.string.host));
        //  tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.legle, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabThree.setText(getString(R.string.declare));
        //  tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.legle, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabThree);

    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();

        for (int j = 0; j < tabsCount; j++) {

            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {

                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {

                    Typeface customFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/" + "SanFranciscoDisplay-Regular_0.ttf");
                    ((TextView) tabViewChild).setTypeface(customFont, Typeface.NORMAL);
                }
            }
        }
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public class Pager extends FragmentStatePagerAdapter {

        //integer to count number of tabs
        int tabCount;

        private String[] tabTitles = new String[]{getString(R.string.visitor), getString(R.string.host), getString(R.string.declare)};

        //Constructor to the class
        public Pager(FragmentManager fm, int tabCount) {
            super(fm);
            //Initializing tab count
            this.tabCount = tabCount;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        //Overriding method getItem
        @Override
        public Fragment getItem(int position) {
            //Returning the current tabs
            switch (position) {
                case 0:
                    return VisitorViewFragment.newInstance(layout_info_panel, subtitle);
                case 1:

                    return HostFragment.newInstance(layout_info_panel, subtitle);

                case 2:
                    return DeclareFragment.newInstance(layout_info_panel, subtitle);

                default:
                    return null;
            }
        }

        //Overriden method getCount to get the number of tabs
        @Override
        public int getCount() {
            return tabCount;
        }
    }
}

