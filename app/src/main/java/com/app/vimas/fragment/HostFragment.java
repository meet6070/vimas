package com.app.vimas.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.vimas.R;

public class HostFragment extends Fragment {

    RelativeLayout layout_info_panel;
    TextView subtitle;

    // TODO: Rename and change types and number of parameters
    public static HostFragment newInstance(RelativeLayout layout_info_panel, TextView subtitle) {
        HostFragment fragment = new HostFragment();
        fragment.layout_info_panel = layout_info_panel;
        fragment.subtitle = subtitle;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_host, container, false);

        return view;
    }
}
