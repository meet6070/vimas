package com.app.vimas.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.vimas.R;

public class DeclareFragment extends Fragment {
    RelativeLayout layout_info_panel;
    TextView subtitle;

    public static DeclareFragment newInstance(RelativeLayout layout_info_panel, TextView subtitle) {
        DeclareFragment fragment = new DeclareFragment();
        fragment.layout_info_panel = layout_info_panel;
        fragment.subtitle = subtitle;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_declare, container, false);
    }

}
