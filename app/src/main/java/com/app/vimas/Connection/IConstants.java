package com.app.vimas.Connection;

public interface IConstants {

    //ERROR LABLE
    String DEBUG = "Debug";
    String WS_NAMESPACE = "http://tempuri.org/";
    String NO_RECORD = "No Records";
    String SERVER_URL = "http://103.75.190.118/ws_vimas/gcheck.asmx";
    String Description = "Description";
    String BLANK_STRING = "";

    //ERROR CODES FOR HANDLERS
    int ERR_SERVER_NOT_RESPONSE = 1;
    int ERR_NETWORK_NOT_AVAILABLE = 2;
    int ERR_INTERAL = 6;

    String Main="Main";
    String Settings="Settings";
    String name="name";
    String forms="forms";
    String form="form";
    String _Count="_Count";
    String fields="fields";
    String field="field";

}
