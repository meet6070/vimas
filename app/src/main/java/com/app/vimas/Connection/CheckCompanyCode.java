package com.app.vimas.Connection;

import android.app.Activity;
import android.util.Log;

import com.app.vimas.Utils;

import org.json.JSONException;

import java.net.ConnectException;
import java.net.SocketException;
import java.util.HashMap;
import java.util.Map;

public class CheckCompanyCode {
    public String checkCompanyCode(Activity activity, String code) throws Exception {
        //System.out.println("LOCK EXCEPTION :: ClientInfo Service - insertUpdateNewClients - start");

        SOAPCaller soapCaller = null;
        String response = "", object;
        Map<String, Object> params = null;
        try {
            soapCaller = SOAPCaller.getInstance();
            params = new HashMap<>();
            params.put("srv", "Vimas");
            params.put("deviceid", "1234564849878979");
            params.put("code", code);

            if (Utils.isNetworkAvailable(activity)) {
                response = soapCaller.getString("CompanyCode", params);
            } else {
                throw new ConnectException();
            }
        } catch (JSONException e) {
            Log.e(IConstants.DEBUG, "e");
            throw e;
        } catch (ConnectException con) {
            Log.e(IConstants.DEBUG, "e");
            throw con;
        } catch (SocketException se) {
            Log.e(IConstants.DEBUG, "e");
            throw se;
        } catch (Exception e) {
            Log.e(IConstants.DEBUG, "e");
            throw e;
        } finally {
            soapCaller = null;
            if (params != null) {
                params.clear();
            }
        }
        return response;
    }
}
