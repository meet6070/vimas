package com.app.vimas.Connection;

import android.content.Context;
import android.util.Log;

import com.app.vimas.Utils;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;



/**
 * A singleton Utilsity class for calling soap based web services. This class have different methods to call soap based web services as per the need.
 *
 * @author Ishan Dave
 */


public class SOAPCaller {
    private static SOAPCaller singleInstance;
    String ws_Url;
    Context mContext;

    private SOAPCaller() {
    }

    public static SOAPCaller getInstance() {
        if (singleInstance == null) singleInstance = new SOAPCaller();

        return singleInstance;
    }

    public String getString(final String methodName, Map<String, Object> params) throws MalformedURLException, XmlPullParserException, ConnectException, SocketException, SocketTimeoutException, IOException, Exception {
        ws_Url = IConstants.SERVER_URL;
        if (ws_Url.length() > 0 || ws_Url != null) {
            Log.e("the web url ", ws_Url);
        }
        HttpTransportSE httpTransportSE = new HttpTransportSE(ws_Url, 200000);

        String result = null;
        String resultJSON = "";

        try {
            SoapObject request = new SoapObject(IConstants.WS_NAMESPACE, methodName);
            Log.e("Soap namespace & method", IConstants.WS_NAMESPACE + "" + methodName);
            if (params != null && params.size() > 0) {
                Set<String> keys = params.keySet();
                Iterator<String> iterator = keys.iterator();
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    request.addProperty(key, params.get(key));

                    //System.out.println("Value of keys"+key);
                    //System.out.println("Value of keysValue"+params.get(key));
                    key = null;
                }
            }
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            envelope.setOutputSoapObject(request);

            envelope.dotNet = true;

            envelope.implicitTypes = true;


            Object responseSoapObj = null;   //added by rafay while loop.
            while (responseSoapObj == null) {
                httpTransportSE.call(IConstants.WS_NAMESPACE + methodName, envelope); //Socket Exception

                Log.e(methodName, " called");
                responseSoapObj = envelope.getResponse();
            }
            if (responseSoapObj != null) {
                if (responseSoapObj instanceof SoapObject) {
                    result = ((SoapObject) responseSoapObj).getProperty(0).toString();
                } else if (responseSoapObj instanceof SoapPrimitive) {
                    result = ((SoapPrimitive) responseSoapObj).toString();
                    Log.e("Soap c ", "result recieved");
                }
            }
//			Log.i("json object", result+"");


            if (result != null && !result.equals(IConstants.BLANK_STRING) && !result.equals(IConstants.NO_RECORD)) {
                try {
                    resultJSON = result;
                    Log.e("Soap", "result converted to json result");
                } catch (Exception e) {
                    Log.e("WS::ERROR", methodName + ">>Method name and result>>" + result + " >>param>>" + params.toString());
                    throw e;
                }
            }

            return resultJSON;
        } catch (MalformedURLException malformedURLException) {
            Log.e("exception from soap ", IConstants.SERVER_URL);
            Log.e(IConstants.DEBUG, Utils.getStackTrace(malformedURLException));
            throw malformedURLException;
        } catch (XmlPullParserException xpe) {
            if (xpe != null)
                Log.e(IConstants.DEBUG, Utils.getStackTrace(xpe));
            throw xpe;
        } catch (ConnectException con) {
            Log.e(IConstants.DEBUG, Utils.getStackTrace(con));
            throw con;
        } catch (SocketTimeoutException e) {
            Log.e(IConstants.DEBUG, Utils.getStackTrace(e));
            throw e;
        } catch (SocketException se) {
            Log.e(IConstants.DEBUG, Utils.getStackTrace(se));
            throw se;
        } catch (IOException ioe) {
            Log.e(IConstants.DEBUG, Utils.getStackTrace(ioe));
            throw ioe;
        } catch (Exception exception) {
            Log.e(IConstants.DEBUG, Utils.getStackTrace(exception));
            throw exception;
        }
    }
}