package com.app.vimas.utils;

import android.util.Log;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Created by Lovepreet on 21/9/17.
 */

public class XmlParser extends DefaultHandler {

//    List<XmlValuesModel> list=null;

    public String response = "";
    // string builder acts as a buffer
    String xml;
    StringBuilder builder;

    public void XmlParser(String xml) {
        this.xml = xml;
    }


    // Initialize the arraylist
    // @throws SAXException

    @Override
    public void startDocument() throws SAXException {

        /******* Create ArrayList To Store XmlValuesModel object ******/

    }


    // Initialize the temp XmlValuesModel object which will hold the parsed info
    // and the string builder that will store the read characters
    // @param uri
    // @param localName ( Parsed Node name will come in localName  )
    // @param qName
    // @param attributes
    // @throws SAXException

    @Override
    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {

        /****  When New XML Node initiating to parse this function called *****/

        // Create StringBuilder object to store xml node value
        builder = new StringBuilder();

        if (localName.equals("string")) {
            response = builder.toString();

            //Log.i("parse","====login=====");
        }

    }


    @Override
    public void endElement(String uri, String localName, String qName)
            throws SAXException {


        if (localName.equalsIgnoreCase("string")) {
            response = builder.toString();
            Log.i("res", response);
        }

        /** finished reading a job xml node, add it to the arraylist **/


        // Log.i("parse",localName.toString()+"========="+builder.toString());
    }


    // Read the value of each xml NODE
    // @param ch
    // @param start
    // @param length
    // @throws SAXException

    @Override
    public void characters(char[] ch, int start, int length)
            throws SAXException {

        /******  Read the characters and append them to the buffer  ******/
        String tempString = new String(ch, start, length);
        builder.append(tempString);
    }
}
