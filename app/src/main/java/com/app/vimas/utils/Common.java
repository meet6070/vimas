package com.app.vimas.utils;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.vimas.R;

import java.io.File;


/**
 * Created by hiteshsheth on 29/08/17.
 */
public class Common {


    public static final int TYPE_PROGRAMMATICALLY = 0;
    ;
    public static final int TYPE_XML = 1;
    ;
    //LegalDetails
    public static boolean flag_vehical = false, flag_lic_font = false, flag_lic_back = false, flag_lic_ADS = false;
    public static File userimage = null, file_vehicalregi1 = null, file_licensefront1 = null, file_licenseback1 = null, file_licenseADS1 = null;
    public static String strfile_vehicalregi, strfile_licensefront, strfile_licenseback, strfile_licenseADS;
    public static Uri mCapturedImageURI, mCapturedUserImageURI;
    public static String CountryCode = "44";
    public static String struserImage;
    public static ViewPager viewPager;
    public static RelativeLayout rlMainView;
    public static TextView tvTitle;
    public static Activity signupActivity;
    //Edit Profile details
    //LegalDetails
    public static boolean editflag_vehical = false, editflag_lic_font = false, editflag_lic_back = false, editflag_lic_ADS = false;
    public static File edituserimage = null, editfile_vehicalregi = null, editfile_licensefront = null, editfile_licenseback = null, editfile_licenseADS = null;
    public static String editstruserImage, editstrfile_vehicalregi, editstrfile_licensefront, editstrfile_licenseback, editstrfile_licenseADS;
    public static Uri mEditCapturedImageURI, mEditCapturedUserImageURI;
    public static Activity mainActivity;
    public static RelativeLayout rlEditMainView;
    public static TextView tvEditTitle;
    public static ViewPager editviewPager;
    public static String device_token = "";
    public static String device_refresh_token = "";
    public static String TAG = "Ucar Driver";
    public static boolean signupflag = false;

    public static boolean driverflag = false;
    public static boolean isplay = true;

    public static double latitude = 0.0;
    public static double longitude = 0.0;

    public static String name = "", username = "", email = "", mobile = "", password = "", confirmpassword = "", dob = "", address = "",
            gender = "", vehicalmake = "", vehicalmodel = "", vehicalno = "", vehicalcapacity = "", vehicalfacility = "", drivinglicense = "", licenseplate = "",
            insurance = "", licenseexpirydate = "", csv_type = "";

    public static String v_name = "";

//    public static File file_vehicalregi = null, file_licensefront = null, file_licenseback = null;

    public static String vehicaltype = "", vehicaltypeid = "";


    /*Editprofile Flag*/
    public static String strUserimage = "", strVehicalregi = "", strLicensefront = "", strLicenseback = "", strLicenseADS = "";

    public static boolean userimageflag = false, vehicalregiflag = false, licensefrontflag = false,
            licensebackflag = false, editparsanal = false, editvehical = false, editlegal = false;


    public static int Moving = 0;


    public static AlarmManager mgrAlarm;
    public static String zone_name = "";

    public static boolean isNetworkAvailable(Activity act) {

        ConnectivityManager connMgr = (ConnectivityManager) act.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            // fetch data
            return true;
        } else {
            // display error
            return false;
        }

    }

    /*public static void showInternetInfo(final Activity act, String message) {
        if (!act.isFinishing()) {
            final InternetInfoPanel mk = new InternetInfoPanel(act, InternetInfoPanel.InternetInfoPanelType.MKInfoPanelTypeInfo, "SUCCESS!", message, 2000);
            mk.show();
            mk.getIv_ok().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (mk.isShowing() && !act.isFinishing())
                            mk.cancel();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }*/

    public static boolean ShowHttpErrorMessage(Activity activity, String ErrorMessage) {

        Log.d("ErrorMessage", "ErrorMessage = " + ErrorMessage);
        boolean Status = true;
        if (ErrorMessage != null && !ErrorMessage.equals("")) {
            if (ErrorMessage.contains("Connect to")) {
                //Common.showInternetInfo(activity, "");
                Status = false;
            } else if (ErrorMessage.contains("failed to connect to")) {
                //Common.showInternetInfo(activity, "");
                Status = false;
            } else if (ErrorMessage.contains("Internal Server Error")) {
                Common.showMkError(activity, "Internal Server Error");
                Status = false;
            } else if (ErrorMessage.contains("Request Timeout")) {
                Common.showMkError(activity, "Request Timeout");
                Status = false;
            } else if (ErrorMessage.contains("Request Timeout")) {
                Common.showMkError(activity, "Something Went Wrong ");
                Status = false;
            }
        } else {
            // Toast.makeText(activity, "Server Time Out", Toast.LENGTH_LONG).show();
            Common.showMkError(activity, "Something Went Wrong ");
            Status = false;
        }
        return Status;
    }

    public static void showMkSucess(final Activity act, String message, String isHeader) {
        if (!act.isFinishing()) {

            Animation slideUpAnimation;

            final Dialog MKInfoPanelDialog = new Dialog(act, android.R.style.Theme_Translucent_NoTitleBar);

            MKInfoPanelDialog.setContentView(R.layout.mkinfopanel);
            MKInfoPanelDialog.show();
            RelativeLayout layout_info_panel = (RelativeLayout) MKInfoPanelDialog.findViewById(R.id.layout_info_panel);

            layout_info_panel.setVisibility(View.VISIBLE);
            slideUpAnimation = AnimationUtils.loadAnimation(act.getApplicationContext(),
                    R.anim.slide_up_map);
            slideUpAnimation.setFillAfter(true);
            slideUpAnimation.setDuration(4000);

            layout_info_panel.setBackgroundResource(R.color.sucess_color);
            layout_info_panel.startAnimation(slideUpAnimation);

            if (isHeader.equals("yes")) {
                RelativeLayout.LayoutParams buttonLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) act.getResources().getDimension(R.dimen._48sdp));
                buttonLayoutParams.setMargins(0, (int) act.getResources().getDimension(R.dimen._48sdp), 0, 0);
                layout_info_panel.setLayoutParams(buttonLayoutParams);
            }

            TextView subtitle = (TextView) MKInfoPanelDialog.findViewById(R.id.subtitle);
            subtitle.setText(message);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (MKInfoPanelDialog.isShowing() && !act.isFinishing())
                            MKInfoPanelDialog.cancel();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 2000);

        }
    }

    public static void showMkHitMessage(final Activity act, String message) {
        if (!act.isFinishing()) {

            Animation slideUpAnimation;

            final Dialog MKInfoPanelDialog = new Dialog(act, android.R.style.Theme_Translucent_NoTitleBar);

            MKInfoPanelDialog.setContentView(R.layout.mkinfopanel);

            final RelativeLayout layout_info_panel = (RelativeLayout) MKInfoPanelDialog.findViewById(R.id.layout_info_panel);

            layout_info_panel.setVisibility(View.VISIBLE);

            MKInfoPanelDialog.show();
            slideUpAnimation = AnimationUtils.loadAnimation(act.getApplicationContext(),
                    R.anim.slide_up_map);

            layout_info_panel.startAnimation(slideUpAnimation);

            layout_info_panel.setBackgroundResource(R.color.dialog_hit_color);

            TextView subtitle = (TextView) MKInfoPanelDialog.findViewById(R.id.subtitle);
            subtitle.setText(message);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (MKInfoPanelDialog.isShowing() && !act.isFinishing()) {
                            MKInfoPanelDialog.cancel();
                            layout_info_panel.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 2000);

        }
    }

    public static void showMkErrorMessage(final Activity act, String message) {
        if (!act.isFinishing()) {

            Animation slideUpAnimation;

            final Dialog MKInfoPanelDialog = new Dialog(act, android.R.style.Theme_Translucent_NoTitleBar);

            MKInfoPanelDialog.setContentView(R.layout.mkinfopanel);

            final RelativeLayout layout_info_panel = (RelativeLayout) MKInfoPanelDialog.findViewById(R.id.layout_info_panel);

            layout_info_panel.setVisibility(View.VISIBLE);

            MKInfoPanelDialog.show();
            slideUpAnimation = AnimationUtils.loadAnimation(act.getApplicationContext(),
                    R.anim.slide_up_map);

            layout_info_panel.startAnimation(slideUpAnimation);

            layout_info_panel.setBackgroundResource(R.color.dialog_error_color);

            TextView subtitle = (TextView) MKInfoPanelDialog.findViewById(R.id.subtitle);
            subtitle.setText(message);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (MKInfoPanelDialog.isShowing() && !act.isFinishing()) {
                            MKInfoPanelDialog.cancel();
                            layout_info_panel.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 2000);

        }
    }

    public static void showMKPanelError(final Activity act, String message, final RelativeLayout rlMainView, TextView tvTitle) {
        if (!act.isFinishing() && (rlMainView.getVisibility() == View.GONE)) {
            if ((rlMainView.getVisibility() == View.GONE)) {
                rlMainView.setVisibility(View.VISIBLE);
            }

            rlMainView.setBackgroundResource(R.color.dialog_error_color);
            tvTitle.setText(message);
            Animation slideUpAnimation = AnimationUtils.loadAnimation(act.getApplicationContext(), R.anim.slide_up_map);
            rlMainView.startAnimation(slideUpAnimation);

        }
    }

    public static void ValidationGone(final Activity activity, final RelativeLayout rlMainView, EditText edt_reg_username) {
        edt_reg_username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //  Log.d("charSequence", "charSequence = " + charSequence.length() + "==" + rlMainView.getVisibility() + "==" + View.VISIBLE);
                if (charSequence.length() > 0 && rlMainView.getVisibility() == View.VISIBLE) {
                    if (!activity.isFinishing()) {
                        TranslateAnimation slideUp = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -100);
                        slideUp.setDuration(10);
                        slideUp.setFillAfter(true);
                        rlMainView.startAnimation(slideUp);
                        slideUp.setAnimationListener(new Animation.AnimationListener() {

                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                rlMainView.setVisibility(View.GONE);
                            }
                        });

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public static void showMkError(final Activity act, final String error_code) {
        String message = "";
        if (!act.isFinishing()) {
            Log.d("error_code", "error_code = " + error_code);

            if (error_code.equals("1")) {
                message = act.getResources().getString(R.string.forgotpassword_1);
            } else if (error_code.equals("2")) {
                message = act.getResources().getString(R.string.forgotpassword_2);
            } else if (error_code.equals("7")) {
                message = act.getResources().getString(R.string.register_validation1);
            } else if (error_code.equals("8")) {
                message = act.getResources().getString(R.string.register_validation2);
            } else if (error_code.equals("9")) {
                message = act.getResources().getString(R.string.register_validation3);
            } else if (error_code.equals("10")) {
                message = act.getResources().getString(R.string.register_validation4);
            } else if (error_code.equals("11")) {
                message = act.getResources().getString(R.string.register_validation5);
            } else if (error_code.equals("12") || error_code.equals("18")) {
                message = act.getResources().getString(R.string.register_validation6);
            } else if (error_code.equals("13")) {
                message = act.getResources().getString(R.string.register_validation7);
            } else if (error_code.equals("14")) {
                message = act.getResources().getString(R.string.register_validation8);
            } else if (error_code.equals("17")) {
                message = act.getResources().getString(R.string.register_validation5);
            } else if (error_code.equals("19")) {
                message = act.getResources().getString(R.string.register_validation19);
            } else if (error_code.equals("21")) {
                message = act.getResources().getString(R.string.register_validation9);
            } else if (error_code.equals("20")) {
                message = act.getResources().getString(R.string.register_validation20);
            } else if (error_code.equals("23")) {
                message = act.getResources().getString(R.string.usercanceltrip);
            } else if (error_code.equals("1000")) {
                message = act.getResources().getString(R.string.manegDriver1000);
            } else {
                message = act.getResources().getString(R.string.error);
            }

            final SharedPreferences userPref = PreferenceManager.getDefaultSharedPreferences(act);

            Animation slideUpAnimation;

            final Dialog MKInfoPanelDialog = new Dialog(act, android.R.style.Theme_Translucent_NoTitleBar);

            MKInfoPanelDialog.setContentView(R.layout.mkinfopanel);
            MKInfoPanelDialog.show();
            slideUpAnimation = AnimationUtils.loadAnimation(act.getApplicationContext(),
                    R.anim.slide_up_map);

            RelativeLayout layout_info_panel = (RelativeLayout) MKInfoPanelDialog.findViewById(R.id.layout_info_panel);
            layout_info_panel.startAnimation(slideUpAnimation);

            RelativeLayout.LayoutParams buttonLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) act.getResources().getDimension(R.dimen._40sdp));
            buttonLayoutParams.setMargins(0, (int) act.getResources().getDimension(R.dimen._50sdp), 0, 0);
            layout_info_panel.setLayoutParams(buttonLayoutParams);

            TextView subtitle = (TextView) MKInfoPanelDialog.findViewById(R.id.subtitle);
            subtitle.setText(message);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (MKInfoPanelDialog.isShowing() && !act.isFinishing())
                            MKInfoPanelDialog.cancel();

                        if (error_code.equals("1") || error_code.equals("5")) {
                            SharedPreferences.Editor editor = userPref.edit();
                            editor.clear();
                            editor.commit();

//                            Intent logInt = new Intent(act, LoginOptionActivity.class);
//                            logInt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            logInt.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            logInt.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            act.startActivity(logInt);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 2000);

        }
    }

    public static void ShowFormMkPanel(Activity activity, final RelativeLayout rlMainView, TextView tvTitle, String message, String errorCode) {

        if (!activity.isFinishing() && rlMainView.getVisibility() == View.GONE) {
            rlMainView.setAlpha(1f);
            rlMainView.setVisibility(View.VISIBLE);
            Log.e("TaxiBooking", "show error layout" + errorCode);
            int ColorCode = 0;
            if (errorCode.equals("error")) {
                ColorCode = activity.getResources().getColor(R.color.mk_error);
            } else if (errorCode.equals("success")) {
                ColorCode = activity.getResources().getColor(R.color.sucess_color);
            } else if (errorCode.equals("info")) {
                ColorCode = activity.getResources().getColor(R.color.info_color);
            }
            rlMainView.setBackgroundColor(ColorCode);
            tvTitle.setText(message);

            Animation slideUpAnimation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.slide_up_mkinfo);
            rlMainView.startAnimation(slideUpAnimation);

            if (errorCode.equals("success") || errorCode.equals("error") && rlMainView.getVisibility() == View.VISIBLE) {
                if (!activity.isFinishing()) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            TranslateAnimation slideUp = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -100);
                            slideUp.setDuration(2000);
                            slideUp.setFillAfter(true);
                            rlMainView.startAnimation(slideUp);
                            slideUp.setAnimationListener(new Animation.AnimationListener() {

                                @Override
                                public void onAnimationStart(Animation animation) {
                                }

                                @Override
                                public void onAnimationRepeat(Animation animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animation animation) {
                                    rlMainView.setVisibility(View.GONE);
                                }
                            });
                        }
                    }, 3000);


                }
            }
        }
    }

    public static void HideFormMkPanel(final Activity activity, final RelativeLayout rlMainView, EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("charSequence", "charSequence = " + charSequence.length() + "==" + rlMainView.getVisibility() + "==" + View.VISIBLE);
                if (charSequence.length() > 0 && rlMainView.getVisibility() == View.VISIBLE) {
                    if (!activity.isFinishing()) {
                        TranslateAnimation slideUp = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -100);
                        slideUp.setDuration(10);
                        slideUp.setFillAfter(true);
                        rlMainView.startAnimation(slideUp);
                        slideUp.setAnimationListener(new Animation.AnimationListener() {

                            @Override
                            public void onAnimationStart(Animation animation) {
                                rlMainView.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                rlMainView.setVisibility(View.GONE);
                            }
                        });

                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    public static Float getTotalPrice(String intailrate, float FirstKm, Float distance, String fromintailrate, String ride_time_rate, int totalTime) {
        Float totlePrice;
        Float firstPrice = Float.parseFloat(intailrate);
        Float secoundPrice = null;
        Log.e(Common.TAG, "Price-Total time 1." + intailrate + "\n2." + FirstKm + "\n3." + distance + "\n4." + fromintailrate + "\n5." + ride_time_rate + "\n6." + totalTime);
        if (distance != 0) {
            Log.e(Common.TAG, "Price-FirstKm:" + FirstKm + "\nDistance:" + distance);
            if (FirstKm < distance) {
                Float afterkm = distance - FirstKm;
                Log.e(Common.TAG, "Price-fromintailrate distance= " + fromintailrate + "==" + afterkm);
                if (fromintailrate.equals(""))
                    fromintailrate = "0";
                secoundPrice = Float.parseFloat(fromintailrate) * afterkm;
                Log.e(Common.TAG, "Price-total price = " + distance + "==" + FirstKm + "==" + afterkm);
            }
        }


        float driverprice = Float.parseFloat(ride_time_rate) * totalTime;
        Log.e(Common.TAG, "Price-Total First and Second with driver price = First:" + firstPrice + "Second:" + secoundPrice + "Driver:" + driverprice);
        if (secoundPrice != null)
            totlePrice = firstPrice + secoundPrice + driverprice;
        else
            totlePrice = firstPrice + driverprice;

        Log.e(Common.TAG, "Price-Totalprice:" + totlePrice);

        return totlePrice;
    }
}
