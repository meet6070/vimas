package com.app.vimas.utils;

import android.util.Log;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.BufferedReader;
import java.io.StringReader;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Lovepreet on 21/9/17.
 */

public class XmlParserInstance {

    public static String getJsonResult(String xml) {

        XmlParser parser = new XmlParser();

        BufferedReader br = new BufferedReader(new StringReader(xml));

        try {
            InputSource is = new InputSource(br);
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser sp = factory.newSAXParser();
            XMLReader reader = sp.getXMLReader();
            reader.setContentHandler(parser);
            reader.parse(is);

        } catch (Exception e) {
            Log.e("EXE", e.toString());
        }

        return parser.response;
    }
}